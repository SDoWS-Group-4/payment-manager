package dtu.manager;

import dtu.PaymentStore;
import dtu.model.Payment;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;

public class PaymentManager {

    PaymentStore paymentStore = PaymentStore.getInstance();

    public void savePayment(String id, Payment payment) {
        try {
            if (paymentStore.getPaymentById(id) == null) {
                paymentStore.setPayments(id, new ArrayList<>(Arrays.asList(payment)));
            } else {
                paymentStore.addPayments(id, payment);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Payment> getPaymentsById(String id) throws NoSuchElementException {
        try {
            if (!paymentStore.getPayments().containsKey(id)) {
                throw new NoSuchElementException();
            }
            return paymentStore.getPaymentById(id);
        } catch (NoSuchElementException | NullPointerException e) {
            throw new NoSuchElementException("No report found");
        }
    }

    public ArrayList<Payment> getPaymentsByReceiverId(String id) throws NoSuchElementException {
        try {
            ArrayList<Payment> temp = new ArrayList<>();
           for (ArrayList<Payment> list : paymentStore.getPayments().values()) {
               list.stream().filter(p -> p.getReceiver().equals(id)).forEach(p -> temp.add(p));;
           }
           return temp;


        } catch (NoSuchElementException | NullPointerException e) {
            throw new NoSuchElementException("No report found");
        }
    }

    public HashMap<String, ArrayList<Payment>>  getAllPayments() {
        return paymentStore.getPayments();
    }
}
