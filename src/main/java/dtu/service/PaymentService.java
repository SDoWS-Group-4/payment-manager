package dtu.service;

import dtu.manager.PaymentManager;
import dtu.model.CorrelationId;
import dtu.model.Customer;
import dtu.model.Merchant;
import dtu.model.Payment;
import messaging.Event;
import messaging.MessageQueue;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

import javax.sound.sampled.SourceDataLine;

public class PaymentService {

    MessageQueue queue;
    private Map<CorrelationId, CompletableFuture<String>> correlations = new ConcurrentHashMap<>();
    private Map<CorrelationId, CompletableFuture<Customer>> correlationsCustomer = new ConcurrentHashMap<>();
    private Map<CorrelationId, CompletableFuture<Merchant>> correlationsMerchant = new ConcurrentHashMap<>();
    private Map<CorrelationId, CompletableFuture<String>> correlationsToken = new ConcurrentHashMap<>();
    private Map<CorrelationId, Payment> payments = new ConcurrentHashMap<>();

    PaymentManager paymentManager = new PaymentManager();

    public PaymentService(MessageQueue q) {
        this.queue = q;
        this.queue.addHandler("PaymentRequested", this::handlePaymentRequested);

        this.queue.addHandler("TokenValidated", this::handleTokenValidated);
        this.queue.addHandler("TokenNotValidated", this::handleTokenNotValidated);

        this.queue.addHandler("MoneyTransfered", this::handleMoneyTransfered);
        this.queue.addHandler("MoneyNotTransfered", this::handleMoneyNotTransfered);

        this.queue.addHandler("CustomerByIdFound", this::handleCustomerByIdFound);
        this.queue.addHandler("CustomerByIdNotFound", this::handleCustomerByIdNotFound);

        this.queue.addHandler("CustomerByDTUIdFound", this::handleCustomerByDTUIdFound);
        this.queue.addHandler("CustomerByDTUIdNotFound", this::handleCustomerByDTUIdNotFound);

        this.queue.addHandler("MerchantByIdFound", this::handleMerchantByIdFound);
        this.queue.addHandler("MerchantByIdNotFound", this::handleMerchantByIdNotFound);

        this.queue.addHandler("CustomerPaymentsRequested", this::handleCustomerPaymentsRequested);
        this.queue.addHandler("MerchantPaymentsRequested", this::handleMerchantPaymentsRequested);

        this.queue.addHandler("AllPaymentsRequested", this::handleAllPaymentsRequested);
        
    }

    public void handleTokenValidated(Event ev) {     // What shoud this be responsible for?
        var dtuPayid = ev.getArgument(0, String.class); //Getting id
        var correlationId = ev.getArgument(1, CorrelationId.class);

        correlationsToken.get(correlationId).complete(dtuPayid);
    }

    private void handleTokenNotValidated(Event ev) {
        var ex = ev.getArgument(0, Exception.class);
        var correlationId = ev.getArgument(1, CorrelationId.class);

        correlationsToken.get(correlationId).completeExceptionally(ex);
    }

    private void handleMerchantByIdNotFound(Event ev) {
        var ex = ev.getArgument(0, NoSuchElementException.class);
        var correlationId = ev.getArgument(1, CorrelationId.class);

        correlationsMerchant.get(correlationId).completeExceptionally(ex);
    }

    private void handleMerchantByIdFound(Event ev) {
        var merchant = ev.getArgument(0, Merchant.class);
        var correlationId = ev.getArgument(1, CorrelationId.class);

        correlationsMerchant.get(correlationId).complete(merchant);
    }

    private void handleCustomerByIdNotFound(Event ev) {
        var ex = ev.getArgument(0, NoSuchElementException.class);
        var correlationId = ev.getArgument(1, CorrelationId.class);

        correlationsCustomer.get(correlationId).completeExceptionally(ex);
    }

    private void handleCustomerByIdFound(Event ev) {
        var customer = ev.getArgument(0, Customer.class);
        var correlationId = ev.getArgument(1, CorrelationId.class);

        correlationsCustomer.get(correlationId).complete(customer);
    }

    private void handleCustomerByDTUIdNotFound(Event ev) {
        var ex = ev.getArgument(0, NoSuchElementException.class);
        var correlationId = ev.getArgument(1, CorrelationId.class);

        correlationsCustomer.get(correlationId).completeExceptionally(ex);
    }

    private void handleCustomerByDTUIdFound(Event ev) {
        var customer = ev.getArgument(0, Customer.class);
        var correlationId = ev.getArgument(1, CorrelationId.class);

        correlationsCustomer.get(correlationId).complete(customer);
    }


    public void handlePaymentRequested(Event ev) {     // What shoud this be responsible for?
        var payment= ev.getArgument(0, Payment.class); //Getting id
        var correlationId = ev.getArgument(1, CorrelationId.class);
        Event event;
        try {
            // event = new Event("TransferMoneyRequested", new Object[]{payment,
            // correlationId});
            // queue.publish(event);

            var tokenCorr = CorrelationId.randomId();
            correlationsToken.put(tokenCorr, new CompletableFuture<>());
            Event tokenEvent = new Event("ValidateToken", new Object[] { payment.getSender(), tokenCorr });
            queue.publish(tokenEvent);
            var customerId = correlationsToken.get(tokenCorr).join();

            var merchantCorrelationId = CorrelationId.randomId();
            correlationsMerchant.put(merchantCorrelationId, new CompletableFuture<>());
            Event merchantEvent = new Event("MerchantByIdRequested", new Object[] { payment.getReceiver(), merchantCorrelationId });
            queue.publish(merchantEvent);
            var merchant = correlationsMerchant.get(merchantCorrelationId).join();

            var customerCorrelationId = CorrelationId.randomId();
            correlationsCustomer.put(customerCorrelationId, new CompletableFuture<>());
            Event customerEvent = new Event("CustomerByDTUIdRequested", new Object[] { customerId, customerCorrelationId });
            queue.publish(customerEvent);
            Customer customer = correlationsCustomer.get(customerCorrelationId).join();

            System.out.println("Merchant DTUPay id: " +merchant.getId());
            System.out.println("Merchant Bank account id: " + merchant.getBankAccount());
            payment.setID(payment.getSender()); //GetSender() is token on this line
            System.out.println("Just set the token as ID: " + payment.getID());
            payment.setSender(customer.getBankAccount());
            payment.setReceiver(merchant.getBankAccount());

            // paymentManager.makePayment(payment.getID(), payment);

            // Checking if id has given token and returning bool
            event = new Event("TransferMoneyRequested", new Object[] { payment, correlationId });
            queue.publish(event);
            
            //For saving payment with correct values
            payment.setSender(customerId);
            payment.setReceiver(merchant.getId());
            payments.put(correlationId, payment);

        } catch (Exception ex) {

            if (ex.getCause() != null) {
                System.out.println(ex.getCause().getMessage());
                Event ExceptionEvent = new Event("PaymentNotSuccessful",
                        new Object[] { new Exception(ex.getCause().getMessage()), correlationId });
                queue.publish(ExceptionEvent);
            } else {
                System.out.println(ex.getMessage());
                Event ExceptionEvent = new Event("PaymentNotSuccessful",
                        new Object[] { new Exception(ex.getMessage()), correlationId });
                queue.publish(ExceptionEvent);
            }
        }
    }

    public void handleMoneyTransfered(Event ev) {
        var correlationId = ev.getArgument(0, CorrelationId.class);
        Event event;

        // paymentManager.makePayment(payment.getID(), payment);
        event = new Event("PaymentSuccessful", new Object[] { "success", correlationId });

        
        Payment savedPayment = payments.get(correlationId);
        paymentManager.savePayment(savedPayment.getSender(), savedPayment);
        System.out.println("Just saved this payment: " + savedPayment.getSender() + savedPayment.toString());
        System.out.println("Getting payment" + paymentManager.getPaymentsById(savedPayment.getSender()).toString());
        queue.publish(event);
    }

    public void handleMoneyNotTransfered(Event ev) {
        var ex = ev.getArgument(0, Exception.class);
        var correlationId = ev.getArgument(1, CorrelationId.class);
        Event event;

        // paymentManager.makePayment(payment.getID(), payment);

        event = new Event("PaymentNotSuccessful", new Object[] { ex, correlationId });

        queue.publish(event);
    }

    private void handleCustomerPaymentsRequested(Event ev) {
        var id = ev.getArgument(0, String.class);
        var correlationId = ev.getArgument(1, CorrelationId.class);
        Event event;
        try {
            event = new Event("CustomerPaymentsFound", 
            new Object[] { paymentManager.getPaymentsById(id), correlationId });
        } catch (NoSuchElementException e) {
            event = new Event("CustomerPaymentsNotFound", 
            new Object[] { e, correlationId });
        }
        queue.publish(event);
    }

    private void handleAllPaymentsRequested(Event ev) {
        var correlationId = ev.getArgument(0, CorrelationId.class);
        Event event;
        try {
            event = new Event("AllPaymentsFound", 
            new Object[] { paymentManager.getAllPayments(), correlationId });
        } catch (NoSuchElementException e) {
            event = new Event("AllPaymentsNotFound", 
            new Object[] { e, correlationId });
        }
        queue.publish(event);

    }

    private void handleMerchantPaymentsRequested(Event ev) {
        var id = ev.getArgument(0, String.class);
        var correlationId = ev.getArgument(1, CorrelationId.class);
        Event event;
        try {
            event = new Event("MerchantPaymentsFound", 
            new Object[] { paymentManager.getPaymentsByReceiverId(id), correlationId });
        } catch (NoSuchElementException e) {
            event = new Event("MerchantPaymentsNotFound", 
            new Object[] { e, correlationId });
        }
        queue.publish(event);
    }
}
