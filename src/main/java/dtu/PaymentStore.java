package dtu;

import dtu.model.Payment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.NoSuchElementException;

public class PaymentStore {

    private HashMap<String, ArrayList<Payment>> payments = new HashMap<>();
    private static PaymentStore single_instance = null;

    public ArrayList<Payment> getPaymentById(String id) {
        return payments.get(id);
    }

    public PaymentStore() {
        Payment paym = new Payment("asda212414", "123456789", "1234567891", 20.50);
        payments.put("123456789", new ArrayList<>(Arrays.asList(paym)));
        Payment paym2 = new Payment("asda212414", "123456789", "1234567891", 20.50);
        payments.put("1234567893", new ArrayList<>(Arrays.asList(paym2)));
        Payment paym3 = new Payment("asda212414", "123456789", "1234567891", 20.50);
        payments.put("1234567891", new ArrayList<>(Arrays.asList(paym3)));
        Payment paym4 = new Payment("asda212414", "123456789", "1234567891", 20.50);
        payments.put("1234567892", new ArrayList<>(Arrays.asList(paym4)));
    }

    public HashMap<String, ArrayList<Payment>> getPayments() {
        return payments;
    }

    public HashMap<String, ArrayList<Payment>> getPaymentsId() {
        return payments;
    }

    public void setPayments(String id, ArrayList<Payment> payments) {
        this.payments.put(id, payments);
    }

    public void addPayments(String id, Payment payment) {
        this.payments.get(id).add(payment);
    }

    public static PaymentStore getInstance() {
        if (single_instance == null)
            single_instance = new PaymentStore();

        return single_instance;
    }
}